#create tabel employee
create table employee(
	id int(3) auto_increment,
	nama varchar(20) not null,
	atasan_id int(3) not null,
	company_id int(3) not null,
	constraint employee_pk
		primary key (id)
);
#create tabel company
create table company(
    id int(3) auto_increment,
    nama varchar(20),
    alamat varchar(20),
    constraint company_pk
        primary key (id)
);
#insert into employee
insert into employee (nama, atasan_id, company_id)
VALUES ('Pak Budi','NULL','1'),
       ('Pak Tono','1','1'),
       ('Pak Totok','1','1'),
       ('Bu Sinta','2','1'),
       ('Bu Novi','3','1'),
       ('Andre','4','1'),
       ('Dono','4','1'),
       ('Ismir','5','1'),
       ('Anto','5','1');

#insert into company

insert into company (nama,alamat) values ('PT JAVAN','Sleman'),('PT DICODING','Bandung');

#query cari CEO
select employee.nama, atasan_id, company.nama as Company from employee join company  on company_id = company.id
where atasan_id is null ;

#cari staf
select A.id,A.nama,A.atasan_id,C.nama As company from employee as A left join employee as B on A.id=B.atasan_id
join company C on A.company_id = C.id
    where B.id is null;

#cari direktur
select A.id,A.nama as nama_direktur,B.nama as atasan,C.nama as company from employee as A join employee as B on A.atasan_id=B.id
join company C on A.company_id = C.id
where B.id=1;

#cari manager
select A.id,A.nama as nama_direktur,B.nama as atasan,C.nama as company from employee as A join employee as B on A.atasan_id=B.id
join company C on A.company_id = C.id
where B.id=2 or B.id=3;

#cari jumlah bawahan
with recursive bawahan (id, nama, atasan_id, company_id) as (
  select * from employee where nama = 'Pak Budi'
  union
  select p.* from employee p inner join bawahan on p.atasan_id= bawahan.id
)select count(id) -1 as jumlah_bawahan from bawahan;
