insert into tbl_karyawan (nama, jenis_kelamin, status, tanggal_lahir, tanggal_masuk, departement) values
                            ('Rizki saputra','L','Menikah','1980-11-10','2011-01-01','1'),
                            ('Farhan Reza','L','Lajang','1989-01-11','2011-01-01','1'),
                             ('Riyando Aldi','L','Menkah','1977-01-26','2011-01-01','1'),
                             ('Diego Manuel','L','Menikah','1983-02-22','2012-09-04','2'),
                             ('Satya Laksana','L','Menikah','1981-01-02','2013-03-19','2'),
                             ('Miguel Hernandez','L','Menikah','1994-10-16','2014-05-15','2'),
                             ('Putri Persada','P','Menikah','1988-01-30','2013-04-14','2'),
                             ('Alma Safira','P','Menikah','1991-05-18','2012-01-22','3'),
                             ('Haqi Hafiz','L','Lajang','1995-09-19','2015-03-09','3'),
                             ('Abi Isyawara','L','Lajang','1991-06-03','2012-01-03','3'),
                             ('Maman Kresna','L','Lajang','1993-08-21','2012-09-15','3'),
                             ('Nadia Aulia','P','Lajang','1989-10-07','2012-05-07','4'),
                             ('Mutiara Reski','P','Menikah','1988-03-23','2013-05-21','4'),
                             ('Dani Setiawan','L','Lajang','1986-02-11','2014-11-30','4'),
                             ('Budi Putra','L','Lajang','1995-10-23','2015-12-03','4');

insert into tbl_departement (nama) values ('Management'),('Pengembangan Bisnis'),('Teknisi'),('Analisi');