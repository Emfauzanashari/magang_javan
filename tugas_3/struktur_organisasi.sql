# keterangan :
# jabatan 1 = Direktur
# jabatan 2 = Manager
# jabatan 3 = Staf

# mengambil data semua karyawan di bawah manager
select * from tbl_karyawan where jabatan=3;
# mengambil staff di bawah manager farhan (staf penembangan bisnis dan teknisi)

select tbl_karyawan.id,tbl_karyawan.nama as nama,jenis_kelamin,status,
       tanggal_lahir,tanggal_masuk,jabatan,tbl_departement.nama as departement
from tbl_karyawan join tbl_departement on tbl_karyawan.departement=tbl_departement.id
where jabatan=3 && departement=2 or departement=3;

# mengambil staff dibawah manager Riyando adi
select tbl_karyawan.id,tbl_karyawan.nama as nama,jenis_kelamin,status,
       tanggal_lahir,tanggal_masuk,jabatan,tbl_departement.nama as departement
from tbl_karyawan join tbl_departement on tbl_karyawan.departement=tbl_departement.id where jabatan=3 && departement=4;