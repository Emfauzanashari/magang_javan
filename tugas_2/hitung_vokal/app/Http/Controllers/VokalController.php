<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\View;

class VokalController extends Controller
{
    public function index()
    {
        return view('vokal');
    }

    public function proses(Request $request)
    {
   
        $kata=$request->kata;
        $kata=strtolower($kata);
  $arr = str_split($kata);
  $vocal = ['a', 'i', 'u', 'e', 'o'];
  $cek = array_intersect($vocal, $arr);
  $len = array_count_values($arr);
  $result = [];
  foreach ($cek as $item) {
    $result[$item] = $len[$item];
}
  
        return view('proses',['kata'=>$result]);

    


}
}
