<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hitung Vokal</title>
</head>
<body>
<form method="post" action="/proses">
    
      <label>Masukan Kata</label>
      <br>
      <textarea name="kata" placeholder="Masukan Kata" required maxlength="20" minlength="4"></textarea>
      <br><br>
      <input type="submit" value="Hitung">
      {{ csrf_field() }}
  </form>
</body>
</html>