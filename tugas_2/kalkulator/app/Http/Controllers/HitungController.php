<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HitungController extends Controller
{
    public function index()
    {
        return view('kalkulator');
    }

    public function proses(Request $request)
    {
        $string=$request->string1;
        $string2=$request->string2;
        $operator1=$request->tambah;
        $operator2=$request->kurang;
        $operator3=$request->kali;
        $operator4=$request->bagi;

        if($operator1==NULL && $operator2==NULL && $operator3==NULL){
            $hasil=$string/$string2;
        }elseif($operator1==NULL && $operator2==NULL && $operator4==NULL){
            $hasil=$string * $string2;
        }elseif($operator1==NULL && $operator3==NULL && $operator4==NULL){
            $hasil=$string - $string2;
        }else {
            $hasil=$string + $string2;
        }
        

        return view('proses',['hasil'=>$hasil]);
    }
}
