<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function proses(Request $request)
    {
        $bil1=$request->angka1;
        $bil2=$request->angka2;

        if ($bil1 < $bil2) {
            for ($i=$bil1; $i <=$bil2 ; $i++) { 
            if ($i % 2==0) {
               
               $a="Bilangan adalah  bilangan Genap";

$x[$i]=$a;
                
            }else{
               
               $a="Bilangan  adalah  bilangan Ganjil";

               $x[$i]=$a;
                
            }

        }
      
        return view('proses',['data'=>$x]);
    }
    
    elseif ($bil1 > $bil2) {
        for ($i=$bil1; $i >=$bil2 ; $i--) { 
            if ($i % 2==0) {
                $a="Bilangan  adalah bilangan Genap";
                $x[$i]=$a;
               ;
            }else{
                $a="Bilangan adalah  bilangan Ganjil";
                $x[$i]=$a;
               
            }
    
        }
        return view('proses',['data'=>$x]);
    }
    else{
        echo "Bilangan Tidak Boleh Sama";
    }
    
    }
}
