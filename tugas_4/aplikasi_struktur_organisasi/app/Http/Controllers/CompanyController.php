<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index(){
        $company=DB::table ('company')->get();
    
        // var_dump($company);
        // dd();
        return view('company/company',['company'=>$company]);

    }

    public function tambah(){

        return view('company/tambah');
    }

    public function buat(Request $request){
        DB::table ('company')->insert([
            'nama'=>$request->nama,
            'alamat'=>$request->alamat

        ]);
        return redirect ('/company');
    }

    public function edit($id){
        $company=DB::table('company')
        ->where('id',$id)
        ->get();

        return view('company/edit',['company'=>$company]);
    }

    public function update(Request $request){
        DB::table('company')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);
        // alihkan halaman ke halaman employee
        return redirect('/company');
    }
    public function hapus($id){
        // menghapus data employee berdasarkan id yang dipilih
        DB::table('company')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman employe
        return redirect('/company');
    }

}
