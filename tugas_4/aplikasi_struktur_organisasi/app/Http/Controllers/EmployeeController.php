<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use App\Models\EmployeeModel;

class EmployeeController extends Controller
{
    public function index(){
        $employees=DB::table('employee as A')
        ->leftjoin('employee as B', 'A.atasan_id','B.id')
        ->join ('company as C', 'A.company_id', 'C.id')
        ->select('A.id','A.nama','B.nama As atasan','C.nama as company')
        ->get();
    //    var_dump($employees);
    //    dd();
        return view('employee',['employee'=>$employees]);
    }
    public function tambah(){
        $atasan=DB::table('employee as A')
        -> leftjoin('employee as B', 'A.id','B.atasan_id')
        ->where ('B.id','!=','NULL')
        ->select('A.id as id','A.nama as nama')
        ->DISTINCT('id','nama')
        ->get();
        // var_dump($atasan);
        // dd();
        $company=DB::table('company')
        ->select('id','nama')
        ->get();
        return view('tambah',['atasan'=>$atasan, 'company'=>$company]);
    }
    public function create(Request $request){

        DB::table('employee')->insert([
            'nama'=>$request->nama,
            'atasan_id'=>$request->atasan,
            'company_id'=>$request->company

        ]);
    return redirect ('/');
        // $nama=$request->nama;
        // $atasan_id=$request->atasan;
        // $company_id=$request->company;

        // var_dump($nama);
        // dd();
    }
    public function edit($id){
        // mengambil data employee berdasarkan id yang dipilih
        $employee = DB::table('employee as A')
        ->leftjoin('employee as B', 'A.atasan_id','B.id')
        ->join ('company as C', 'A.company_id', 'C.id')
        ->select('A.id','A.nama','B.id as atasan_id','B.nama As atasan','C.id as company_id','C.nama as company')
        ->where('A.id',$id)
        ->get();

        $atasan=DB::table('employee')
        ->select('id','nama')
        ->get();
        // var_dump($atasan);
        // dd();
        $company=DB::table('company')
        ->select('id','nama')
        ->get();
        // passing data pegawai yang didapat ke view edit.blade.php
        return view('edit',['employee' => $employee,'atasan'=>$atasan,'company'=>$company]);
    }

    public function update(Request $request){
	// update data employee
	DB::table('employee')->where('id',$request->id)->update([
		'nama' => $request->nama,
		'atasan_id' => $request->atasan,
		'company_id' => $request->company
	]);
	// alihkan halaman ke halaman employee
	return redirect('/');
}
    public function hapus($id){
	// menghapus data employee berdasarkan id yang dipilih
	DB::table('employee')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman employe
	return redirect('/');
}
}
