<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah</title>
</head>
<body>
<h1>Tambah Data Employee</h1>
    <form action="/create" method="post" >
    <table>
        <tr>
            <th>
            <label for="nama">Nama</label>
            </th>
            <th>
            <input type="text" name="nama">
            </th>
        </tr>
        <tr>
            <th>
            <label for="atasan_id">Atasan</label>
            </th>
            <th>
            <select name="atasan" id="atasan">
            <option value="">--</option>
            <?php foreach ($atasan as $i):?>
                <option value={{$i->id}} >{{$i->nama}}</option>
            <?php endforeach; ?>
            </select>
            </th>
        </tr>
        <tr>
            <th>
            <label for="company_id">Company</label>
            </th>
            <th>
            <select name="company" id="company">
            <option value="">--</option>
            <?php foreach ($company as $i):?>
                <option value={{$i->id}} >{{$i->nama}}</option>
            <?php endforeach; ?>
            </select>
            </th>
        </tr>
        <tr>
            <th></th>
            <th>
            <input type="submit" value="Tambah">
            </th>
        </tr>
    </table>
            {{ csrf_field() }}
    </form>
</body>
</html>