<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>employee</title>
</head>
<body>
<h1>tabel data karyawan </h1>
<button><a href="/tambah">Tambah Karyawan</a></button>
<br>
    <table border=1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nama</th>
                <th>Atasan</th>
                <th>Company</th>
                <th>Aksi</th>
            </tr>
        </thead>

       <?php foreach ($employee as $i):?>
       <tbody>
            <tr>
                <td>{{$i->id}}</td>
                <td>{{$i->nama}}</td>
                <td>{{$i->atasan}}</td>
                <td>{{$i->company}}</td>
                <td>
                    <button><a href="/edit/{{$i->id}}">Edit</a></button>
                    <button><a href="/hapus/{{ $i->id }}">Hapus</a></button>
                </td>
            </tr>
       </tbody>
       <?php endforeach; ?>
    </table>
</body>
</html>