<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data</title>
</head>
<body>
<?php foreach ($employee as $a):?>
    <form action="/update" method="post" >
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $a->id }}">
        <label for="nama">Nama</label>
        <input type="text" name="nama" value="{{$a->nama}}">
        <br><br>
        <label for="atasan_id">Atasan</label>
        <select name="atasan" id="atasan">
        <option value="{{$a->atasan_id}}">{{$a->atasan}}</option>
        <?php foreach ($atasan as $i):?>
            <option value={{$i->id}} >{{$i->nama}}</option>
        <?php endforeach; ?>
        </select>
        <br><br>
        <label for="company_id">Company</label>
        <select name="company" id="company">
        <option value="{{$a->company_id}}">{{$a->company}}</option>
        <?php foreach ($company as $i):?>
            <option value={{$i->id}} >{{$i->nama}}</option>
        <?php endforeach; ?>
        </select>
            <br><br>
            <input type="submit" value="Update Data">
    
    
    </form>
        <?php endforeach; ?>
</body>
</html>