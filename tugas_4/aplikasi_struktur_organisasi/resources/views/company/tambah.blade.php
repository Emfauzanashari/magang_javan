<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah</title>
</head>
<body>
    <h1>Tambah Data Company</h1>
    <br>
    <form action="/company/buat" method="post">
    {{ csrf_field() }}
    <table>
        <tr>
            <th>
                <label for="nama">Nama Company</label>
            </th>
            <th>
                <input type="text" name="nama">
            </th>
        </tr>

        <tr>
            <th>
                <label for="alamat">Alamat</label>
            </th>
            <th>
                <input type="text" name="alamat">
            </th>
        </tr>
            
        <tr>
        <th></th>
            <th>
                <input type="submit" value="Tambah Data">
            </th>
        </tr>
    </table>
    </form>
   
</body>
</html>