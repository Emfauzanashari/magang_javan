<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Company</title>
</head>
<body>
<h1>Tabel Data Company</h1>

<button><a href="/company/tambah">Tambah Data</a></button>
<br>
    <table border=1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Aksi</th>
            </tr>
        </thead>
    <?php foreach ($company as $i):?>
        <tbody>
            <tr>
                <td>{{$i->id}}</td>
                <td>{{$i->nama}}</td>
                <td>{{$i->alamat}}</td>
                <td>
                    <button><a href="/company/edit/{{$i->id}}">Edit</a></button>
                    <button><a href="/company/hapus/{{$i->id}}">Hapus</a></button>
                </td>
            </tr>
        </tbody>
    <?php endforeach;?>
    </table>
</body>
</html>