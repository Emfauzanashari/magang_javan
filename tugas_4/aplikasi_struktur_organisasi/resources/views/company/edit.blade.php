<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah</title>
</head>
<body>
    <h1>Edit Data Company</h1>
    <br>
    <form action="/company/update" method="post">
    
    <?php foreach ($company as $i):?>
    <table>
    <input type="hidden" name="id" value="{{ $i->id }}">
        <tr>
            <th>
                <label for="nama">Nama Company</label>
            </th>
            <th>
                <input type="text" name="nama" value="{{$i->nama}}">
            </th>
        </tr>

        <tr>
            <th>
                <label for="alamat">Alamat</label>
            </th>
            <th>
                <input type="text" name="alamat" value="{{$i->alamat}}">
            </th>
        </tr>
            
        <tr>
        <th></th>
            <th>
                <input type="submit" value="Update Data">
            </th>
        </tr>
    </table>
    <?php endforeach;?>
    {{ csrf_field() }}
    </form>
   
</body>
</html>