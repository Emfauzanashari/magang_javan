<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// routes employee
Route::get('/', 'EmployeeController@index');
Route::get('/tambah', 'EmployeeController@tambah');
Route::post('/create', 'EmployeeController@create');
Route::get('/edit/{id}', 'EmployeeController@edit');
Route::post('/update','EmployeeController@update');
Route::get('/hapus/{id}','EmployeeController@hapus');

// routes Company
Route::get('/company', 'CompanyController@index');
Route::get('/company/tambah', 'CompanyController@tambah');
Route::post('/company/buat', 'CompanyController@buat');
Route::get('/company/edit/{id}', 'CompanyController@edit');
Route::post('/company/update','CompanyController@update');
Route::get('/company/hapus/{id}','CompanyController@hapus');
